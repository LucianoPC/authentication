$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "authentication/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "authentication"
  s.version     = Authentication::VERSION
  s.authors     = ["Luciano Prestes Cavalcanti"]
  s.email       = ["lucianopcbr@gmail.com"]
  s.homepage    = "https://gitlab.com/LucianoPC/game_issues"
  s.summary     = "Engine to makes authentication."
  s.description = "This engine its used to creates user and your session."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5.0.7"
  s.add_dependency "bcrypt", "~> 3.1.7"
  s.add_dependency "apipie-rails"
  s.add_dependency "active_model_serializers", "~> 0.10.0"

  s.add_development_dependency "sqlite3"
  s.add_development_dependency "rspec-rails", "~> 3.7"
end
