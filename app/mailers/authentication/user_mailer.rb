module Authentication
  class UserMailer < Authentication::ApplicationMailer

    #TODO Fix host variable
    def registration_confirmation(user)
      @user = user

      to = "#{user.name} <#{user.email}>"
      subject = 'Registration Confirmation'

      # host = "#{request.protocol}#{request.host}:#{request.port}"
      host = 'http://localhost:3000'
      @email_confirmation_link = "#{host}/users/confirm_email/#{user.email_confirm_token}"

      mail(to: to, subject: subject)
    end
  end
end
