require_dependency "authentication/api_controller"

module Authentication
  class SessionController < ApiController

    ERROR_LOGIN_OR_PASSWORD = 'Error with your login or password'
    ERROR_EMAIL_NOT_CONFIRMED = 'Confirm your email before login'


    before_action :require_login, only: [:destroy]
    before_action :set_session_params, only: [:create]


    api :POST, '/login', 'Create session and generating the new user token'
    param :username, String, desc: 'The username of the user', required: true
    param :password, String, desc: 'The password of the user', required: true
    error code: 401, desc: SessionController::ERROR_LOGIN_OR_PASSWORD
    error code: 401, desc: SessionController::ERROR_EMAIL_NOT_CONFIRMED
    def create
      username = @session_params[:username]
      password = @session_params[:password]

      if user = User.valid_login?(username, password)
        if user.email_confirmed
          user.login
          render json: Authentication::UserSerializer.new(user).as_json
        else
          render_unauthorized(ERROR_EMAIL_NOT_CONFIRMED)
        end
      else
        render_unauthorized(ERROR_LOGIN_OR_PASSWORD)
      end
    end

    api :DELETE, '/logout', 'Destroy session and remove the user token'
    description ApiController::AUTHENTICATION_REQUIRED_DESCRIPTION
    error code: 401, desc: ApiController::ERROR_ACCESS_DENIED
    def destroy
      @current_user.logout
      head :ok
    end


    private

    def set_session_params
      @session_params = params.permit(:username, :password)
    end
  end
end
