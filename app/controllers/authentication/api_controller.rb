require_dependency "authentication/application_controller"

module Authentication
  class ApiController < ApplicationController
    include ActionController::HttpAuthentication::Token::ControllerMethods

    ERROR_ACCESS_DENIED = 'Access denied'
    AUTHENTICATION_REQUIRED_DESCRIPTION = <<-EOS
      === Authentication required
       Authentication token has to be passed as HTTP header(AUTH-TOKEN).
    EOS


    def require_login
      @current_user = authenticate_token
      @current_user || render_unauthorized(ERROR_ACCESS_DENIED)
    end


    protected

    def render_unauthorized(message)
      errors = {error: message}
      render json: errors, status: :unauthorized
    end


    private

    def authenticate_token
      authenticate_with_http_token do |token, options|
        if user = User.with_unexpired_token(token, 2.days.ago)
          ActiveSupport::SecurityUtils.secure_compare(
            ::Digest::SHA256::hexdigest(token),
            ::Digest::SHA256::hexdigest(user.token))

          user
        end
      end
    end
  end
end
