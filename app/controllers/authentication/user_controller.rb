require_dependency "authentication/api_controller"

module Authentication
  class UserController < ApiController

    before_action :require_login, only: [:update, :destroy]

    def_param_group :user do
      param :user, Hash, desc: 'Hash with user fields', required: true do
        param :name, String, desc: 'The name of the user', required: true
        param :email, String, desc: 'The email of the user', required: true
        param :username, String, desc: 'The username of the user', required: true
        param :password, String, desc: 'The password of the user', required: true
      end
    end

    api :POST, '/users', 'Create new user'
    param_group :user
    def create
      @user = User.new(user_params)

      if @user.save
        UserMailer.registration_confirmation(@user).deliver
        render json: @user, status: :created
      else
        render json: @user.errors, status: :unprocessable_entity
      end
    end

    api :PATCH, '/users', 'Edit user'
    param_group :user
    description ApiController::AUTHENTICATION_REQUIRED_DESCRIPTION
    error code: 401, desc: ApiController::ERROR_ACCESS_DENIED
    def update
      if @current_user.update(user_params)
        render json: @current_user, status: :ok
      else
        render json: @current_user.errors, status: :unprocessable_entity
      end
    end

    api :DELETE, '/users', 'Delete user'
    description ApiController::AUTHENTICATION_REQUIRED_DESCRIPTION
    error code: 401, desc: ApiController::ERROR_ACCESS_DENIED
    def destroy
      @current_user.destroy
      head :ok
    end

    api :GET, '/users/confirm_email/:email_confirm_token', 'Comfirm user email'
    param :email_confirm_token, String, desc: 'Confirm token', required: true
    def confirm_email
      @user = User.find_by(email_confirm_token: params[:email_confirm_token])

      if @user
        @user.email_activate
        render json: @user, status: :ok
      else
        render status: 404
      end
    end


    private

    def user_params
      params.require(:user).permit(:name, :email, :username, :password)
    end
  end
end
