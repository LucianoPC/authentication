module Authentication
  class User < ApplicationRecord

    ### Attributes
    # name                :string
    # email               :string
    # token               :string
    # password            :string
    # username            :string
    # email_confirmed     :boolean
    # token_created_at    :date_time
    # email_confirm_token :string

    before_create :generate_email_confirm_token

    has_secure_token
    has_secure_password

    validates_presence_of :name, :email, :username
    validates_uniqueness_of :username


    def self.valid_login?(username, password)
      user = User.find_by(username: username)
      if user && user.authenticate(password)
        user
      end
    end

    def self.with_unexpired_token(token, period)
      where(token: token).where('token_created_at >= ?', period).first
    end


    def login
      regenerate_token
      touch(:token_created_at)
    end

    def logout
      invalidate_token
    end

    def email_activate
      self.email_confirmed = true
      self.email_confirm_token = nil
      save!(validate: false)
    end

    private

    def invalidate_token
      update_columns(token: nil)
      touch(:token_created_at)
    end

    def generate_email_confirm_token
      if self.email_confirm_token.blank?
        self.email_confirm_token = SecureRandom.urlsafe_base64.to_s
      end
    end
  end
end
