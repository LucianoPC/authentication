module Authentication
  class Engine < ::Rails::Engine
    isolate_namespace Authentication

    initializer :append_migrations do |app|
      unless app.root.to_s.match(root.to_s)
        config.paths["db/migrate"].expanded.each do |paths|
          app.config.paths["db/migrate"] << paths
        end
      end
    end
  end
end
