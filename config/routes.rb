Authentication::Engine.routes.draw do
  apipie

  match '/login', to: 'session#create', via: [:post]
  match '/logout', to: 'session#destroy', via: [:delete]

  match '/users', to: 'user#create', via: [:post]
  match '/users', to: 'user#update', via: [:patch, :put]
  match '/users', to: 'user#destroy', via: [:delete]
  match '/users/confirm_email/:email_confirm_token', to: 'user#confirm_email', via: [:get]
end
