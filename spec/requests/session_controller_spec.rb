require 'rails_helper'

RSpec.describe 'Authentication::Session', type: :request do
  context 'create' do
    it 'success' do
      user = Authentication::User.create!(name: 'Test User', email: 'testuser@test.com',
                          username: 'testuser', password: '123456')

      user.email_activate

      post '/login', params: {username: 'testuser', password: '123456'}

      user_token = Authentication::User.find_by(username: 'testuser').token
      response_token = response.parsed_body['token']

      expect(response.status).to eq(200)
      expect(response_token).to eq(user_token)
    end

    it 'error by username' do
      Authentication::User.create!(name: 'Test User', email: 'testuser@test.com',
                   username: 'testuser', password: '123456')

      post '/login', params: {username: 'invalid', password: '123456'}

      response_error = response.parsed_body['error']

      expect(response.status).to eq(401)
      expect(response_error).to eq(Authentication::SessionController::ERROR_LOGIN_OR_PASSWORD)
    end

    it 'error by password' do
      Authentication::User.create!(name: 'Test User', email: 'testuser@test.com',
                   username: 'testuser', password: '123456')

      post '/login', params: {username: 'testuser', password: 'invalid'}

      response_error = response.parsed_body['error']

      expect(response.status).to eq(401)
      expect(response_error).to eq(Authentication::SessionController::ERROR_LOGIN_OR_PASSWORD)
    end

    it 'error by email activation' do
      Authentication::User.create!(name: 'Test User', email: 'testuser@test.com',
                   username: 'testuser', password: '123456')

      post '/login', params: {username: 'testuser', password: '123456'}

      response_error = response.parsed_body['error']

      expect(response.status).to eq(401)
      expect(response_error).to eq(Authentication::SessionController::ERROR_EMAIL_NOT_CONFIRMED)
    end
  end

  context 'destroy' do
    it 'success' do
      user = Authentication::User.create!(name: 'Test User', email: 'testuser@test.com',
                          username: 'testuser', password: '123456')

      user.login

      delete '/logout', headers: {'Authorization' => "Token #{user.token}"}

      user.reload

      expect(response.status).to eq(200)
      expect(nil).to eq(user.token)
    end

    it 'error by invalid token' do
      user = Authentication::User.create!(name: 'Test User', email: 'testuser@test.com',
                          username: 'testuser', password: '123456')

      token = 'invalid'
      user.login

      delete '/logout', headers: {'Authorization' => "Token #{token}"}

      response_error = response.parsed_body['error']

      expect(response.status).to eq(401)
      expect(response_error).to eq(Authentication::ApiController::ERROR_ACCESS_DENIED)
    end

    it 'error by without header' do
      user = Authentication::User.create!(name: 'Test User', email: 'testuser@test.com',
                          username: 'testuser', password: '123456')

      user.login

      delete '/logout'

      response_error = response.parsed_body['error']

      expect(response.status).to eq(401)
      expect(response_error).to eq(Authentication::ApiController::ERROR_ACCESS_DENIED)
    end
  end
end
