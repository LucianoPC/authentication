require 'rails_helper'

RSpec.describe 'Authentication::User', type: :request do
  context 'create' do
    it 'success' do
      params = {user: {name: 'Test User', email: 'testuser@test.com',
                       username: 'testuser', password: '123456'}}

      post '/users', params: params

      response_user = response.parsed_body

      expect(response.status).to eq(201)
      expect(Authentication::User.count).to eq(1)

      expect(Authentication::User.last.name).to eq(params[:user][:name])
      expect(Authentication::User.last.email).to eq(params[:user][:email])
      expect(Authentication::User.last.username).to eq(params[:user][:username])

      expect(response_user['name']).to eq(params[:user][:name])
      expect(response_user['email']).to eq(params[:user][:email])
      expect(response_user['username']).to eq(params[:user][:username])
    end

    it 'error by empty params' do
      params = {user: {name: '', email: '', username: '', password: ''}}

      post '/users', params: params

      response_error = response.parsed_body

      expect(response.status).to eq(422)
      expect(Authentication::User.count).to eq(0)

      expect(response_error['name'][0]).to eq("can't be blank")
      expect(response_error['email'][0]).to eq("can't be blank")
      expect(response_error['username'][0]).to eq("can't be blank")
      expect(response_error['password'][0]).to eq("can't be blank")
    end

    it 'error by uniqueness params' do
      user_params = {name: 'test user', email: 'testuser@test.com',
                     username: 'testuser', password: '123456'}
      Authentication::User.create!(user_params)
      params = {user: user_params}

      post '/users', params: params

      response_error = response.parsed_body

      expect(response.status).to eq(422)
      expect(Authentication::User.count).to eq(1)

      expect(response_error['username'][0]).to eq("has already been taken")
    end
  end

  context 'update' do
    it 'success' do
      user = Authentication::User.create!(name: 'Test User', email: 'testuser@test.com',
                          username: 'testuser', password: '123456')
      params = {user: {name: 'New Test User', email: 'newtestuser@test.com',
                       username: 'newtestuser', password: 'new123456'}}

      user.login
      headers = {'Authorization' => "Token #{user.token}"}

      patch '/users', params: params, headers: headers

      response_user = response.parsed_body

      expect(response.status).to eq(200)
      expect(Authentication::User.count).to eq(1)

      expect(Authentication::User.last.name).to eq(params[:user][:name])
      expect(Authentication::User.last.email).to eq(params[:user][:email])
      expect(Authentication::User.last.username).to eq(params[:user][:username])

      expect(response_user['name']).to eq(params[:user][:name])
      expect(response_user['email']).to eq(params[:user][:email])
      expect(response_user['username']).to eq(params[:user][:username])
    end

    it 'error by empty params' do
      user = Authentication::User.create!(name: 'Test User', email: 'testuser@test.com',
                          username: 'testuser', password: '123456')
      params = {user: {name: '', email: '', username: '', password: ''}}

      user.login
      headers = {'Authorization' => "Token #{user.token}"}

      patch '/users', params: params, headers: headers

      response_error = response.parsed_body

      expect(response.status).to eq(422)
      expect(Authentication::User.count).to eq(1)

      expect(response_error['name'][0]).to eq("can't be blank")
      expect(response_error['email'][0]).to eq("can't be blank")
      expect(response_error['username'][0]).to eq("can't be blank")
    end

    it 'error by uniqueness params' do
      user_params = {name: 'test user', email: 'testuser@test.com',
                     username: 'testuser', password: '123456'}
      params = {user: user_params}
      Authentication::User.create!(user_params)
      user = Authentication::User.create!(name: 'New Test User', email: 'newtestuser@test.com',
                          username: 'newtestuser', password: 'new123456')

      user.login
      headers = {'Authorization' => "Token #{user.token}"}

      patch '/users', params: params, headers: headers

      response_error = response.parsed_body

      expect(response.status).to eq(422)
      expect(Authentication::User.count).to eq(2)

      expect(response_error['username'][0]).to eq("has already been taken")
    end

    it 'error by authentication' do
      user = Authentication::User.create!(name: 'Test User', email: 'testuser@test.com',
                          username: 'testuser', password: '123456')
      params = {user: {name: '', email: '', username: '', password: ''}}

      headers = {'Authorization' => "Token invalid_token"}

      patch '/users', params: params, headers: headers

      response_error = response.parsed_body

      expect(response.status).to eq(401)
      expect(response_error['error']).to eq(Authentication::ApiController::ERROR_ACCESS_DENIED)
    end
  end

  context 'destroy' do
    it 'success' do
      user = Authentication::User.create!(name: 'Test User', email: 'testuser@test.com',
                          username: 'testuser', password: '123456')

      user.login
      headers = {'Authorization' => "Token #{user.token}"}

      delete '/users', headers: headers

      expect(response.status).to eq(200)
      expect(Authentication::User.count).to eq(0)
    end

    it 'error by authentication' do
      user = Authentication::User.create!(name: 'Test User', email: 'testuser@test.com',
                          username: 'testuser', password: '123456')

      user.login
      headers = {'Authorization' => "Token invalid_token"}

      delete '/users', headers: headers

      response_error = response.parsed_body

      expect(response.status).to eq(401)
      expect(response_error['error']).to eq(Authentication::ApiController::ERROR_ACCESS_DENIED)
    end
  end

  context 'confirm_email' do
    it 'success' do
      user = Authentication::User.create!(name: 'Test User', email: 'testuser@test.com',
                          username: 'testuser', password: '123456')

      get "/users/confirm_email/#{user.email_confirm_token}"

      user_token = Authentication::User.find_by(username: 'testuser').token
      response_token = response.parsed_body['token']

      expect(response.status).to eq(200)
      expect(response_token).to eq(user_token)
    end

    it 'error by wrong email_confirm_token' do
      get '/users/confirm_email/invalid_email_confirm_token'

      expect(response.status).to eq(404)
    end
  end
end

