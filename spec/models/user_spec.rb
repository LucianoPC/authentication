require 'rails_helper'

RSpec.describe Authentication::User, type: :model do
  context 'create' do
    it 'success' do
      user = Authentication::User.create(name: 'Test User', email: 'testuser@test.com',
                         username: 'testuser', password: '123456')

      expect(Authentication::User.last).to eq(user)
    end

    it 'error by empty fields' do
      user = Authentication::User.create

      errors = ["Password can't be blank", "Name can't be blank",
                "Email can't be blank", "Username can't be blank"]

      expect(user.errors.full_messages).to eq(errors)
    end

    it 'error by repeat username' do
      user_1 = Authentication::User.create(name: 'Test User', email: 'testuser@test.com',
                           username: 'testuser', password: '123456')
      user_2 = Authentication::User.create(name: 'Test User', email: 'testuser@test.com',
                           username: 'testuser', password: '123456')

      errors = ["Username has already been taken"]

      expect(user_2.errors.full_messages).to eq(errors)
    end
  end

  context 'authentication' do
    it 'valid login' do
      user = Authentication::User.create(name: 'Test User', email: 'testuser@test.com',
                         username: 'testuser', password: '123456')

      login_user = Authentication::User.valid_login?('testuser', '123456')

      expect(login_user).to eq(user)
    end

    it 'invalid username' do
      user = Authentication::User.create(name: 'Test User', email: 'testuser@test.com',
                         username: 'testuser', password: '123456')

      login_user = Authentication::User.valid_login?('invalid', '123456')

      expect(login_user).to eq(nil)
    end

    it 'invalid password' do
      user = Authentication::User.create(name: 'Test User', email: 'testuser@test.com',
                         username: 'testuser', password: '123456')

      login_user = Authentication::User.valid_login?('testuser', 'invalid')

      expect(login_user).to eq(nil)
    end

    it 'correct token' do
      user = Authentication::User.create(name: 'Test User', email: 'testuser@test.com',
                         username: 'testuser', password: '123456')

      login_user = Authentication::User.valid_login?('testuser', '123456')

      expect(login_user.token).to eq(user.token)
    end

    it 'logout' do
      user = Authentication::User.create(name: 'Test User', email: 'testuser@test.com',
                         username: 'testuser', password: '123456')

      user.logout

      expect(user.token).to eq(nil)
    end

    it 'allow token to be used only once' do
      user = Authentication::User.create(name: 'Test User', email: 'testuser@test.com',
                         username: 'testuser', password: '123456')

      old_token = user.token
      new_token = user.login

      expect(new_token).not_to eq(old_token)
      expect(new_token).not_to eq(Authentication::User.last.token)
    end

    it 'with unexpired token' do
      user = Authentication::User.create(name: 'Test User', email: 'testuser@test.com',
                         username: 'testuser', password: '123456')

      user.login

      expect(Authentication::User.with_unexpired_token(user.token, 1.days.ago)).to eq(user)
    end
  end
end
