from ruby:2.3

RUN mkdir /usr/src/app/
WORKDIR /usr/src/app/

RUN echo "nameserver 8.8.8.8" >> /etc/resolv.conf

RUN apt-get update && apt-get install -y --no-install-recommends \
    nodejs \
 && rm -rf /var/lib/apt/lists/*

COPY Gemfile /usr/src/app/
COPY authentication.gemspec /usr/src/app/

COPY . /usr/src/app

RUN bundle install

CMD bundle exec rspec
