class CreateAuthenticationUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :authentication_users do |t|
      t.string :name,                null: false, default: ''
      t.string :email,               null: false, default: ''
      t.string :token,               null: true,  default: ''
      t.string :username,            null: false, default: ''
      t.string :password_digest,     null: false, default: ''
      t.string :email_confirm_token, null: true,  default: ''

      t.boolean :email_confirmed,    null: true,  default: false

      t.datetime :token_created_at

      t.timestamps
    end

    add_index :authentication_users, :token, unique: true
    add_index :authentication_users, :username, unique: true
  end
end
